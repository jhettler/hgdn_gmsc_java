# Projects in repository #

1. [Reloading defined features after editing/creating of geometry in GMSC - gmsc.editing project](https://bitbucket.org/jhettler/hgdn_gmsc_java/wiki/gmsc.editing)

2. [Custom actionCondition in GMSC - gmsc.actioncondition project](https://bitbucket.org/jhettler/hgdn_gmsc_java/wiki/gmsc.actioncondition)

2. Select underneath elements in the feature in GMSC - gmsc.selection project
(commit 061f727) description in the code