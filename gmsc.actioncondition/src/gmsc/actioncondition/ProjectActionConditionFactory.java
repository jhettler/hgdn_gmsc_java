package gmsc.actioncondition;

import java.util.Arrays;
import java.util.List;

import com.intergraph.tools.utils.disptach.AbstractActionConditionFactory;
import com.intergraph.tools.utils.disptach.ActionCondition;
import com.intergraph.tools.utils.disptach.ActionDispatcher;

public class ProjectActionConditionFactory extends AbstractActionConditionFactory
{
	@Override
	protected List<ActionCondition> createConditions()
	{
		return Arrays.asList(new ActionCondition[]{new ProjectActionCondition(ActionDispatcher.getInstance())});
	}
}
