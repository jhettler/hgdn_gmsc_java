package gmsc.actioncondition;

import java.util.Map;

import com.intergraph.tools.utils.disptach.ActionCondition;
import com.intergraph.tools.utils.disptach.Dispatching;
import com.intergraph.tools.utils.disptach.RPAction;
import com.intergraph.web.core.kernel.ApplicationContext;

public class ProjectActionCondition extends ActionCondition
{
	
	public ProjectActionCondition(Dispatching dispatching){
		super(dispatching);
	}
   
	@Override
	public boolean isSatisfied(RPAction action, Map<String, String> parameters)
	{
		String projectName = ApplicationContext.getProject().getMetadata().getName();
		
		if (projectName != null) 
			return projectName.equals("ZABAGED"); 
		return false;
	}
}
