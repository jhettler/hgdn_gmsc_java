package gmsc.actioncondition;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.intergraph.tools.ui.DefaultDialog;
import com.intergraph.tools.utils.disptach.RPAction;
import com.intergraph.tools.utils.disptach.annotations.Action;
import com.intergraph.tools.utils.disptach.annotations.ActionLocation;
import com.intergraph.tools.utils.disptach.annotations.Condition;
import com.intergraph.tools.utils.disptach.annotations.Plugin;
import com.intergraph.tools.utils.log.Log;
import com.intergraph.web.core.kernel.ApplicationContext;
import com.intergraph.web.core.kernel.plugin.AbstractPlugin;
import com.intergraph.web.extension.MediaSupport;

import net.miginfocom.swing.MigLayout;


@Plugin(alias = "openInBrowserWithParameters", vendor = "Intergraph CS")
public class OpenBrowserWithParameters extends AbstractPlugin{

	@Action(shortCut = "ctrl shift K", actionLocation = ActionLocation.MENUBARFAVORITS ,conditions={@Condition(actionCondition = ProjectActionConditionFactory.class)})
	public void openInBrowserWithParameters(final RPAction action) throws Exception
	{			
		
		//TODO: Do your staff of action here
		
		String url = "http://www.google.com";
			
		browseInNativeBrowser(url);
					
		@SuppressWarnings("serial")
		DefaultDialog defaultDialog = new DefaultDialog(
				ApplicationContext.getMainFrame(), "Attention!") {

			
			protected List<JButton> getButtons() {
				return Arrays.asList(getCloseButton());
			}

			
			protected boolean isUserInputValid() {
				return true;
			}

			
			protected JComponent getDisplayView() {
				
				JPanel panel = new JPanel(new MigLayout("ins 0", "[][grow]",
						"[][grow]"));
				panel.setOpaque(false);

					panel.add(new JLabel("Google page has been opened "));			

				return panel;

			}
			
		};
		
		defaultDialog.setVisible(true);		
		
		}
					
	
	
	public void browseInNativeBrowser(String url)	{
		try {
			MediaSupport.browse(url);
		} catch (IOException | URISyntaxException e) {
			Log.getLogger().log(Level.SEVERE, "", e);
		}
	}	
}